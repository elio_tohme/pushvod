<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use Illuminate\Support\Facades\Storage;

class File extends Model
{

  use Searchable;

  public $asYouType = true;

  /**
   * Get the indexable data array for the model.
   *
   * @return array
   */
  public function toSearchableArray()
  {
    $array = $this->toArray();

    return $array;
  }
      
	public function moviebyvideo ()
  {
    return $this->belongsTo('App\Movie', 'id', 'video_file');
  }

  public function moviebytrailer ()
  {
    return $this->belongsTo('App\Movie', 'id', 'trailer_id');
  }

  public function moviebyposter ()
  {
    return $this->belongsTo('App\Movie', 'id', 'poster_id');
  }

  public function seriesbytrailer ()
  {
    return $this->belongsTo('App\Series', 'id', 'trailer_id');
  }

  public function seriesbyposter ()
  {
    return $this->belongsTo('App\Series', 'id', 'poster_id');
  }  

  public function episode ()
  {
    return $this->belongsTo('App\Episode', 'id', 'video_file');
  }
}
