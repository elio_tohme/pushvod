<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Movie;
use App\File;
use Illuminate\Support\Facades\Storage;

class PushVideo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $MOVIE_ID;
    protected $PUSH;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($movie_id, $push)
    {
        $this->MOVIE_ID = $movie_id;
        $this->PUSH = $push;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $movie = Movie::find($this->MOVIE_ID);
        $filearray = [];
        $videofile = $movie->video;
        $trailerfile = $movie->trailer;
        $posterfile = $movie->poster;
        if ($videofile != '') {
            array_push($filearray, $videofile);
        }

        if ($trailerfile != '') {
            array_push($filearray, $trailerfile);
        }

        if ($posterfile != '') {
            array_push($filearray, $posterfile);   
        }
        if ($request->input('push')) {
            foreach ($filearray as $file) {
                $filepath = Storage::disk('packed')->url($file->type.'/'.$file->id . '.ts');
                rename($filepath, env('PUSH_DIR').$file->type.'/'.$file->id . '.ts');    
            }
            $movie->pushed = true;
            $movie->save();    
        } else {
            foreach ($filearray as $file) {
                $filepath = Storage::disk('pushed')->url($file->type.'/'.$file->id . '.ts');
                rename(env('PUSH_DIR').$file->type.'/'.$file->id . '.ts', env('PACK_DIR').$file->type.'/'.$file->id . '.ts');    
            }
            $movie->pushed = false;
            $movie->save();
        }
    }
}
    