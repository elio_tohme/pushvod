<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\File;

class PackFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $FILE_ID;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->FILE_ID = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file = File::find($this->FILE_ID);
        $filepath = Storage::disk('videos')->url($file->name);
        $exec = sprintf ("%s %s \"%s\" %s.ts %s 0", 
                        env('PACKEXEC'), 
                        $file->id, 
                        storage_path('app/public/videos/') . ($file->name), 
                        storage_path('app/public/packed/'). $file->type .'/' . $file->id,
                        $file->type == 'movies' ? env('PACKTSPID_VIDEO') : env('PACKTSPID_IMG'));

        $process = new Process($exec);
        $process->run();

        // executes after the command finishes
        while ($process->isRunning()) {
            // waiting for process to finish
        }

        $file->packed = true;
        $file->save();
    }
}
