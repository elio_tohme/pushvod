<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Series extends Model
{
	use Searchable;

    public $asYouType = true;

    protected $hidden = ['poster_id', 'trailer_id'];
    
	// Genres relation
	public function genres () 
    {
        return $this->belongsToMany('App\Genre', 'series_genres', 'series', 'genre');
    }

    // get trailer associated file
    public function trailer ()
    {
        return $this->hasOne('App\File', 'id', 'trailer_id');
    }

	// get poster associated file
    public function poster ()
    {
        return $this->hasOne('App\File', 'id', 'poster_id');
    }

    // season relation
    public function seasons () 
    {
    	return $this->hasMany('App\Season');
    }
}
