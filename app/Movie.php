<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Movie extends Model
{
    use Searchable;

    public $asYouType = true;

    protected $hidden = ['poster_id', 'trailer_id', 'video_file'];    
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'movies';
    
    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        return $array;
    }

    public function genres () 
    {
        return $this->belongsToMany('App\Genre', 'movie_genres', 'movie', 'genre');
    }

    public function video ()
    {
        return $this->hasOne('App\File', 'id', 'video_file');
    }

    public function trailer ()
    {
        return $this->hasOne('App\File', 'id', 'trailer_id');
    }

    public function poster ()
    {
        return $this->hasOne('App\File', 'id', 'poster_id');
    }

}
