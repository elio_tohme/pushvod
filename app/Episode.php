<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
	protected $hidden = ['video_file'];
    // Season Realtion
    public function season ()
    {
    	return $this->belongsTo('App\Season');
    }

     public function video ()
    {
        return $this->hasOne('App\File', 'id', 'video_file');
    }
}
