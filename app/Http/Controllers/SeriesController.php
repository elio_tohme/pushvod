<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Series;
use App\Season;
use App\Episode;
use App\Genre;
use App\File;
use App\Version;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use App\Jobs\PushVideo;

class SeriesController extends Controller
{
    public function index ()
    {
        return view('series');
    }

    public function getSeries (Request $request)
    {
    	if ($request->exists('filter')) {
            $pagination = Series::search($request->input('filter'))->paginate(env('ITEM_PER_PAGE'));
            $pagination->load('genres', 'poster', 'trailer', 'seasons.episodes.video');
            
        } else {
            $query = Series::with('genres', 'poster', 'trailer', 'seasons.episodes.video');
            // handle sort option
            if ($request->has('sort')) {
                // handle multisort
                $sorts = explode(',', $request->sort);
                foreach ($sorts as $sort) {
                    list($sortCol, $sortDir) = explode('|', $sort);
                    $query = $query->orderBy($sortCol, $sortDir);
                }
            } else {
                $query = $query->orderBy('number', 'asc');
            }
            $pagination = $query->paginate(env('ITEM_PER_PAGE'));
        }

        $pagination->appends([
            'sort' => $request->sort,
            'filter' => $request->filter,
            'per_page' => $request->per_page,
        ]);

        return response()->json(
                $pagination
        );
    }

    public function addSeries (Request $request)
    {
        $series = new Series();
        $series->title = $request->input('title');
        $genres = array();
        foreach ($request->input('genres') as $value) {
            array_push($genres, $value['id']);
        }
        $series->poster_id = $request->input('poster.id');
        $series->imdb_id = $request->input('imdbid');
        $series->available = $request->input('available');
        $series->trailer_id = $request->input('trailer.id');
        $series->pg_lvl = $request->input('pglvl');
        $series->price3days = $request->input('price3days');
        $series->original_price = $request->input('originalprice');
        $series->discounted_price = $request->input('discountedprice');
        $series->free_to_vip = $request->input('freetovip');
        $series->audio_language = $request->input('audiolanguage');
        $series->subtitle_language = $request->input('subtitlelanguage');
        $series->recommended = $request->input('recommended');
        $series->directors = $request->input('directors');
        $series->stars = $request->input('stars');
        $series->introduction = $request->input('introduction');
        $series->year = $request->input('year');              
        $series->save();
        $series->genres()->sync($genres);
        return $series;
    }
    
    public function deleteSeries ($id) 
    {
        $series = Series::find($id);
        $series->delete();
    }

    public function updateSeries($id, Request $request) 
    {
        // update file table
        $series = Series::find($id);
        $series->title = $request->input('title');
        $genres = array();
        foreach ($request->input('genres') as $value) {
            array_push($genres, $value['id']);
        }
        $series->poster_id = $request->input('poster.id');
        $series->imdb_id = $request->input('imdbid');
        $series->available = $request->input('available');
        $series->trailer_id = $request->input('trailer.id');
        $series->pg_lvl = $request->input('pglvl');
        $series->price3days = $request->input('price3days');
        $series->original_price = $request->input('originalprice');
        $series->discounted_price = $request->input('discountedprice');
        $series->free_to_vip = $request->input('freetovip');
        $series->audio_language = $request->input('audiolanguage');
        $series->subtitle_language = $request->input('subtitlelanguage');
        $series->recommended = $request->input('recommended');
        $series->directors = $request->input('directors');
        $series->stars = $request->input('stars');
        $series->introduction = $request->input('introduction');
        $series->year = $request->input('year');
        $series->genres()->sync($genres);
        $series->save();
    }

    public function addSeason ($series_id, Request $request) 
    {
    	$series = Series::find($series_id);

        $season = new Season();
        $season->number = $request->input('number');
    	$season->series()->associate($series);
        $season->save();
    }

	public function updateSeason ($id, Request $request) 
    {
        $season = Season::find($id);
        $season->number = $request->input('number');
        $season->save();
    }

    public function deleteSeason ($id) 
    {
        $season = Season::find($id);
        $season->delete();
    }

    public function addEpisode (Request $request) 
    {
        $season = Season::find($request->input('season_id'));
        $episode = new Episode();
        $episode->number = $request->input('number');
        $episode->title = $request->input('title');
        $episode->video_file = $request->input('videofile.id');
        $episode->season()->associate($season);
        $episode->save();
    }

	public function updateEpisode ($id, Request $request) 
	{
        $episode = Episode::find($id);
        $episode->number = $request->input('number');
        $episode->title = $request->input('title');
        $episode->video_file = $request->input('videofile.id');
        $episode->save();
    }

    public function deleteEpisode ($id) 
    {
        $episode = Episode::find($id);
        $episode->delete();
    }

    public function getSeriesInfos ()
    {
    	$genres = Genre::get();
        $files = File::with('moviebyvideo', 'moviebytrailer', 'moviebyposter', 'episode', 'seriesbyposter', 'seriesbytrailer')->get();
        $orphanvideofiles = [];
        $imageorphanfiles = [];
        foreach ($files as $file) {
            if ($file->moviebyvideo == null && $file->moviebytrailer == null && $file->moviebyposter == null && $file->episode == null && $file->seriesbyposter == null && $file->seriesbytrailer == null) {
                if ($file->packed == true) {
                    if ($file->type == 'videos') {
                        array_push($orphanvideofiles, $file);
                    } else {
                        array_push($imageorphanfiles, $file);
                    }    
                }
            }
        }
        $imagefiles = File::where('type', 'images')->get();
        return response()->json([
            'genres' => $genres,
            'videos' => $orphanvideofiles,
            'images' => $imageorphanfiles
        ]);
    }

     public function getEpisodesInfos ()
    {
        $files = File::with('moviebyvideo', 'moviebytrailer', 'episode', 'seriesbytrailer')->get();
        $orphanvideofiles = [];
        foreach ($files as $file) {
            if ($file->moviebyvideo == null && $file->moviebytrailer == null && $file->moviebyposter == null && $file->episode == null && $file->seriesbyposter == null && $file->seriesbytrailer == null) {
                if ($file->packed == true) {
                    if ($file->type == 'videos') {
                        array_push($orphanvideofiles, $file);
                    }  
                }
            }
        }
        $imagefiles = File::where('type', 'images')->get();
        return response()->json([
            'videos' => $orphanvideofiles
        ]);
    }

    public function pushEpisode (Request $request) 
    {
        // PushVideo::dispatch($request->input('id'), $request->input('push'));
        $episode = Episode::find($request->input('id'));
        $filearray = [];
        $videofile = $episode->video;
        if ($videofile != '') {
            array_push($filearray, $videofile);
        }

        if ($request->input('push')) {
            foreach ($filearray as $file) {
                $filepath = Storage::disk('packed')->url($file->type.'/'.$file->id . '.ts');
                rename($filepath, env('PUSH_DIR').$file->type.'/'.$file->id . '.ts');    
            }
            $episode->pushed = true;
            $episode->save();    
        } else {
            foreach ($filearray as $file) {
                $filepath = Storage::disk('pushed')->url($file->type.'/'.$file->id . '.ts');
                rename(env('PUSH_DIR').$file->type.'/'.$file->id . '.ts', env('PACK_DIR').$file->type.'/'.$file->id . '.ts');    
            }
            $episode->pushed = false;
            $episode->save();
        }
    }

    public function pushSeries (Request $request) 
    {
        // PushVideo::dispatch($request->input('id'), $request->input('push'));
        $series = Series::with('poster', 'trailer','seasons.episodes.video')->find($request->input('id'));
        $filearray = [];
        $trailerfile = $series->trailer;
        $posterfile = $series->poster;
    
        if ($trailerfile != '') {
            array_push($filearray, $trailerfile);
        }

        if ($posterfile != '') {
            array_push($filearray, $posterfile);   
        }
        if ($request->input('push')) {
            foreach ($filearray as $file) {
                $filepath = Storage::disk('packed')->url($file->type.'/'.$file->id . '.ts');
                rename($filepath, env('PUSH_DIR').$file->type.'/'.$file->id . '.ts');    
            }
            $series->pushed = true;
            $series->save();    
        } else {
            $videofile = array();
            $seasons = $series->seasons;
            foreach ($seasons as $season) {
                $episodes = $season->episodes; 
                foreach ($episodes as $episode) {
                    $filepath = Storage::disk('pushed')->url($episode->video->type.'/'.$episode->video->id . '.ts');
                    rename(env('PUSH_DIR').$episode->video->type.'/'.$episode->video->id . '.ts', env('PACK_DIR').$episode->video->type.'/'.$episode->video->id . '.ts');
                    $updateEpisode = Episode::find($episode->id);
                    $updateEpisode->pushed = false;
                    $updateEpisode->save();
                }
            }
            
            foreach ($filearray as $file) {
                $filepath = Storage::disk('pushed')->url($file->type.'/'.$file->id . '.ts');
                rename(env('PUSH_DIR').$file->type.'/'.$file->id . '.ts', env('PACK_DIR').$file->type.'/'.$file->id . '.ts');    
            }
            $series->pushed = false;
            $series->save();
        }
    }
}
