<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;

class GenreController extends Controller
{
    //
    public function index ()
    {
    	return view('genres');
    }

    public function getGenres ()
    {
    	return Genre::all();
    }

    public function addGenre (Request $request)
    {
    	$genre =  new Genre();
    	$genre->name = $request->input('name');
    	$genre->save();
    }

    public function deleteGenre ($id)
    {
    	$genre = Genre::find($id);
    	$genre->delete();
    }
}
