<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use App\Jobs\PackFile;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class FileController extends Controller
{
    public function indexUpload ()
    {
        return view('upload');
    }

    public function indexFilesAssigned ()
    {
        return view('file');
    }

    public function indexFilesNotAssigned ()
    {
        return view('unassignedfiles');
    }

    public function uploadFile (Request $request)
    {
    	// determine type of file from its extension
    	$extension = $request->file->extension();
        $type = ($extension == 'png' || $extension == 'jpeg' || $extension == 'jpg') ? 'images' : 'videos';
    	
    	// get uploaded file name 
    	$filename =  $request->file->getClientOriginalName();

    	
    	// create new file to be saved after the file is moved
    	$file = new File();
    	$file->name = $filename; 
    	$file->type = $type; 

    	// move file to corresponding folder in the storage
    	$path = $request->file->storeAs('', $filename, $type);

    	// save eloquent model
    	$file->save();

    	return $path;
    }

    public function getFiles (Request $request, $assigned)
    {
        if ($request->exists('filter')) {
            $pagination = File::search($request->input('filter'))->paginate(env('ITEM_PER_PAGE'));
            $pagination->load('genres', 'video', 'poster', 'trailer');
            
        } else {
            if ($assigned == 1){
                $query = File::with('moviebyvideo', 'moviebytrailer', 'moviebyposter')
                            ->orhas('moviebyvideo')
                            ->orhas('moviebytrailer')
                            ->orhas('moviebyposter');   
            } else {
                $query = File::with('moviebyvideo', 'moviebytrailer', 'moviebyposter')
                            ->doesnthave('moviebyvideo')
                            ->doesnthave('moviebytrailer')
                            ->doesnthave('moviebyposter');
            }
            // handle sort option
            if ($request->has('sort')) {
                // handle multisort
                $sorts = explode(',', $request->sort);
                foreach ($sorts as $sort) {
                    list($sortCol, $sortDir) = explode('|', $sort);
                    $query = $query->orderBy($sortCol, $sortDir);
                }
            } else {
                $query = $query->orderBy('number', 'asc');
            }
            $pagination = $query->paginate(env('ITEM_PER_PAGE'));
            
        }

        $pagination->appends([
            'sort' => $request->sort,
            'filter' => $request->filter,
            'per_page' => $request->per_page
        ]);

        return response()->json(
                $pagination
        );
    }

    public function packFile (Request $request) 
    {
        // PackFile::dispatch($request->input('id'));
        $file = File::find($request->input('id'));
        $exec = sprintf ("%s %s \"%s\" %s.ts %s 0", 
                        env('PACKEXEC'), 
                        $file->id, 
                        storage_path('app/public/' .$file->type).'/' . ($file->name), 
                        storage_path('app/public/packed/'). $file->type .'/' . $file->id,
                        $file->type == 'videos' ? env('PACKTSPID_VIDEO') : env('PACKTSPID_IMG'));

        $process = new Process($exec);
        $process->run();

        // executes after the command finishes
        while ($process->isRunning()) {
            // waiting for process to finish
        }

        $file->packed = true;
        $file->save();

        return $file;
    }

    public function delete ($id) 
    {

        $file = File::find($id);
        Storage::disk('public')->delete($file->type .'/' . ($file->name));
        
        if ($file->packed) {
            Storage::disk('public')->delete('/packed/'. $file->type .'/' . $file->id . '.ts');
        }
        
        $file->delete();
    }

}
