<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\PushVideo;
use App\Movie;
use App\Series;
use App\Genre;
use App\File;
use App\Version;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class MovieController extends Controller
{
    public function index ()
    {
        return view('movies');
    }

    public function getMovies (Request $request)
    {
    	if ($request->exists('filter')) {
            $pagination = Movie::search($request->input('filter'))->paginate(env('ITEM_PER_PAGE'));
            $pagination->load('genres', 'video', 'poster', 'trailer');
            
        } else {
            $query = Movie::with('genres', 'video', 'poster', 'trailer');
            // handle sort option
            if ($request->has('sort')) {
                // handle multisort
                $sorts = explode(',', $request->sort);
                foreach ($sorts as $sort) {
                    list($sortCol, $sortDir) = explode('|', $sort);
                    $query = $query->orderBy($sortCol, $sortDir);
                }
            } else {
                $query = $query->orderBy('number', 'asc');
            }
            $pagination = $query->paginate(env('ITEM_PER_PAGE'));
        }

        $pagination->appends([
            'sort' => $request->sort,
            'filter' => $request->filter,
            'per_page' => $request->per_page,
        ]);

        return response()->json(
                $pagination
        );
    }

    public function addMovie (Request $request)
    {
        $movie = new Movie();
        $movie->title = $request->input('title');
        $genres = array();
        if (count($request->input('genres')) > 0) {
            foreach ($request->input('genres') as $value) {
                array_push($genres, $value['id']);
            }
        }
        $movie->poster_id = $request->input('poster.id');
        $movie->imdb_id = $request->input('imdbid');
        $movie->video_file = $request->input('videofile.id');
        $movie->available = $request->input('available');
        $movie->trailer_id = $request->input('trailer.id');
        $movie->pg_lvl = $request->input('pglvl');
        $movie->price3days = $request->input('price3days');
        $movie->original_price = $request->input('originalprice');
        $movie->discounted_price = $request->input('discountedprice');
        $movie->free_to_vip = $request->input('freetovip');
        $movie->audio_language = $request->input('audiolanguage');
        $movie->subtitle_language = $request->input('subtitlelanguage');
        $movie->recommended = $request->input('recommended');
        $movie->duration = $request->input('duration');
        $movie->directors = $request->input('directors');
        $movie->stars = $request->input('stars');
        $movie->introduction = $request->input('introduction');
        $movie->year = $request->input('year');              
        $movie->save();
        $movie->genres()->sync($genres);
        return $movie;
    }
    
    public function deleteMovie ($id) 
    {
        $movie = Movie::find($id);
        $movie->delete();
    }

    public function updateMovie($id, Request $request) 
    {
        // update file table
        $movie = Movie::find($id);
        $movie->title = $request->input('title');
        $genres = array();
        foreach ($request->input('genres') as $value) {
            array_push($genres, $value['id']);
        }
        $movie->poster_id = $request->input('poster.id');
        $movie->imdb_id = $request->input('imdbid');
        $movie->video_file = $request->input('videofile.id');
        $movie->available = $request->input('available');
        $movie->trailer_id = $request->input('trailer.id');
        $movie->pg_lvl = $request->input('pglvl');
        $movie->price3days = $request->input('price3days');
        $movie->original_price = $request->input('originalprice');
        $movie->discounted_price = $request->input('discountedprice');
        $movie->free_to_vip = $request->input('freetovip');
        $movie->audio_language = $request->input('audiolanguage');
        $movie->subtitle_language = $request->input('subtitlelanguage');
        $movie->recommended = $request->input('recommended');
        $movie->duration = $request->input('duration');
        $movie->directors = $request->input('directors');
        $movie->stars = $request->input('stars');
        $movie->introduction = $request->input('introduction');
        $movie->year = $request->input('year');
        $movie->genres()->sync($genres);
        $movie->save();
    }

    public function getMovieInfos ()
    {
    	$genres = Genre::get();
        $files = File::with('moviebyvideo', 'moviebytrailer', 'moviebyposter', 'episode', 'seriesbyposter', 'seriesbytrailer')->get();
        $orphanvideofiles = [];
        $imageorphanfiles = [];
        foreach ($files as $file) {
            if ($file->moviebyvideo == null && $file->moviebytrailer == null && $file->moviebyposter == null && $file->episode == null && $file->seriesbyposter == null && $file->seriesbytrailer == null) {
                if ($file->packed == true) {
                    if ($file->type == 'videos') {
                        array_push($orphanvideofiles, $file);
                    } else {
                        array_push($imageorphanfiles, $file);
                    }    
                }
            }
        }
        $imagefiles = File::where('type', 'images')->get();
        return response()->json([
            'genres' => $genres,
            'videos' => $orphanvideofiles,
            'images' => $imageorphanfiles
        ]);
    }

    public function pushMovie (Request $request) 
    {
        // PushVideo::dispatch($request->input('id'), $request->input('push'));
        $movie = Movie::find($request->input('id'));
        $filearray = [];
        $videofile = $movie->video;
        $trailerfile = $movie->trailer;
        $posterfile = $movie->poster;
        if ($videofile != '') {
            array_push($filearray, $videofile);
        }

        if ($trailerfile != '') {
            array_push($filearray, $trailerfile);
        }

        if ($posterfile != '') {
            array_push($filearray, $posterfile);   
        }
        if ($request->input('push')) {
            foreach ($filearray as $file) {
                $filepath = Storage::disk('packed')->url($file->type.'/'.$file->id . '.ts');
                rename($filepath, env('PUSH_DIR').$file->type.'/'.$file->id . '.ts');    
            }
            $movie->pushed = true;
            $movie->save();    
        } else {
            foreach ($filearray as $file) {
                $filepath = Storage::disk('pushed')->url($file->type.'/'.$file->id . '.ts');
                rename(env('PUSH_DIR').$file->type.'/'.$file->id . '.ts', env('PACK_DIR').$file->type.'/'.$file->id . '.ts');    
            }
            $movie->pushed = false;
            $movie->save();
        }
    }

    public function pushDB ()
    {
        $version  = Version::first();
        if (empty($version)) {
            $version = new Version();
            $version->push_version = 1;
            $version->save();
        } else {
            $version->push_version = $version->push_version + 1;
            $version->save();
        }

        $movies = Movie::with('genres')->where('pushed', true)->where('available', true)->get();
        $series = Series::with('genres', 'poster', 'trailer', 'seasons.episodes.video')->where('pushed', true)->where('available', true)->get();
        // return $series;
        $data = '<?xml version="1.0" encoding="UTF-8"?>
<pushvod version="' . $version->push_version . '">';
    foreach ($series as $serie) {
        $stringgenre = '';
        foreach ($serie->genres as $index => $genre) {
            if ($index > 0 ){
                $stringgenre .= ' ';
            }
            $stringgenre .= $genre->name;
        }
    $data.= "
    <series id='".$serie->id."' type='series'>
        <title>".$serie->title."</title>
        <year>".$serie->year."</year>
        <PG-Level>".$serie->pg_lvl."</PG-Level>
        <Price-For3Days>".$serie->price3days."</Price-For3Days>
        <Original-Price>".$serie->original_price."</Original-Price>
        <Discounted-Price>".$serie->discounted_price."</Discounted-Price>
        <FreeToVip>".$serie->free_to_vip."</FreeToVip>
        <Genre>".$stringgenre."</Genre>
        <Audio-Language>".$serie->audio_language."</Audio-Language>   
        <Sutitle-Language>".$serie->subtitle_language."</Sutitle-Language>
        <Recommend>".$serie->recommended."</Recommend>
        <Directors>".$serie->directors."</Directors>
        <Stars>".$serie->stars."</Stars>
        <Introduction>".$serie->introduction."</Introduction>
        <PIC-File-ID>".$serie->poster_id."</PIC-File-ID>
        <Trailer-File-ID>".$serie->trailer_id."</Trailer-File-ID>
        <numchunks>1</numchunks>";
    foreach ($serie->seasons as $season) {
        $data.= "
        <Season number='".$season->number."'>";

        foreach ($season->episodes as $episode) {
            $data.= "
            <Episode title='".$episode->title."' number='".$episode->number."' >
                <Video-File-ID>".$episode->video->id."</Video-File-ID>
            </Episode>";
        }
        $data.= "
        </Season>";
    }
    
    $data.="
    </series>";
    }
    foreach ($movies as $movie) {
        $stringgenre = '';
        foreach ($movie->genres as $index => $genre) {
            if ($index > 0 ){
                $stringgenre .= ' ';
            }
            $stringgenre .= $genre->name;
        }
    $data.= "
    <video id='".$movie->id."' type='movie'>
        <title>".$movie->title."</title>
        <year>".$movie->year."</year>
        <duration>".$movie->duration."</duration>
        <PG-Level>".$movie->pg_lvl."</PG-Level>
        <Price-For3Days>".$movie->price3days."</Price-For3Days>
        <Original-Price>".$movie->original_price."</Original-Price>
        <Discounted-Price>".$movie->discounted_price."</Discounted-Price>
        <FreeToVip>".$movie->free_to_vip."</FreeToVip>
        <Genre>".$stringgenre."</Genre>
        <Audio-Language>".$movie->audio_language."</Audio-Language>   
        <Sutitle-Language>".$movie->subtitle_language."</Sutitle-Language>
        <Recommend>".$movie->recommended."</Recommend>
        <Directors>".$movie->directors."</Directors>
        <Stars>".$movie->stars."</Stars>
        <Introduction>".$movie->introduction."</Introduction>
        <Video-File-ID>".$movie->video_file."</Video-File-ID>
        <PIC-File-ID>".$movie->poster_id."</PIC-File-ID>
        <Trailer-File-ID>".$movie->trailer_id."</Trailer-File-ID>
        <numchunks>1</numchunks>
    </video>";
    }

     $data.= '
</pushvod>
    ';
        $filename = 'db';
        Storage::disk('public')->put($filename.'.xml', $data);
        $exec = sprintf ("%s %s \"%s.xml\" %s.ts %s %s", 
                        env('PACKEXEC'), 
                        0, 
                        storage_path('app/public/') . $filename, 
                        env('PUSH_DIR').'db/' . $filename,
                        env('PACKTSPID_XML'),
                        $version->push_version
                    );

        $process = new Process($exec);
        $process->run();

         while ($process->isRunning()) {
            // waiting for process to finish
        }

        return redirect('/page/movies');
       
    }
}
