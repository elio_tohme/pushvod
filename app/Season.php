<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    // Episode relation
    public function episodes () 
    {
    	return $this->hasMany('App\Episode', 'season_id');
    }

    // Searies Realtion
    public function series ()
    {
    	return $this->belongsTo('App\Series', 'series_id');
    }
}
