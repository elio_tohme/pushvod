<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('series', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('imdb_id')->nullable();
            $table->boolean('available')->default(false);
            $table->integer('poster_id')->unsigned()->nullable();
            $table->foreign('poster_id')
                  ->references('id')->on('files')
                  ->onDelete('set null');
            $table->integer('trailer_id')->unsigned()->nullable();
            $table->foreign('trailer_id')
                  ->references('id')->on('files')
                  ->onDelete('set null');
            $table->integer('pg_lvl')->default(0);
            $table->integer('price3days')->default(0);
            $table->integer('original_price')->default(0);
            $table->integer('discounted_price')->default(0);
            $table->boolean('free_to_vip')->default(false);
            $table->string('audio_language')->nullable();
            $table->string('subtitle_language')->nullable();
            $table->boolean('recommended')->default(false);
            $table->boolean('pushed')->default(false);
            $table->longText('directors')->nullable();
            $table->longText('stars')->nullable();
            $table->longText('introduction')->nullable();
            $table->integer('year')->default(0);
            $table->timestamps();
        });

        Schema::create('seasons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number');
            $table->integer('series_id')->unsigned()->nullable();
            $table->foreign('series_id')
                  ->references('id')->on('series')
                  ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('episodes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number');
            $table->string('title');
            $table->integer('video_file')->unsigned()->nullable();
            $table->foreign('video_file')
                  ->references('id')->on('files')
                  ->onDelete('set null');
            $table->integer('season_id')->unsigned();
            $table->foreign('season_id')
                ->references('id')->on('seasons')
                ->onDelete('cascade');
            $table->boolean('pushed')->default(false);
            $table->timestamps();
        });

         Schema::create('series_genres', function (Blueprint $table) {
            $table->integer('series')->unsigned();
            $table->integer('genre')->unsigned();
            $table->primary(['series', 'genre']);
            $table->foreign('series')
                  ->references('id')->on('series')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('genre')
                  ->references('id')->on('genres')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('episodes');
        Schema::dropIfExists('seasons');
        Schema::dropIfExists('series_genres');
        Schema::dropIfExists('series');
    }
}
