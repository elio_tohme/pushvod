<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * movies table is realted to files table by video_file column
         */
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('imdb_id')->nullable();
            $table->integer('video_file')->unsigned()->nullable();
            $table->foreign('video_file')
                  ->references('id')->on('files')
                  ->onDelete('set null');
            $table->boolean('available')->default(false);
            $table->integer('poster_id')->unsigned()->nullable();
            $table->foreign('poster_id')
                  ->references('id')->on('files')
                  ->onDelete('set null');
            $table->integer('trailer_id')->unsigned()->nullable();
            $table->foreign('trailer_id')
                  ->references('id')->on('files')
                  ->onDelete('set null');
            $table->integer('pg_lvl')->default(0);
            $table->integer('price3days')->default(0);
            $table->integer('original_price')->default(0);
            $table->integer('discounted_price')->default(0);
            $table->boolean('free_to_vip')->default(false);
            $table->string('audio_language')->nullable();
            $table->string('subtitle_language')->nullable();
            $table->boolean('recommended')->default(false);
            $table->boolean('pushed')->default(false);
            $table->string('duration')->nullable();
            $table->longText('directors')->nullable();
            $table->longText('stars')->nullable();
            $table->longText('introduction')->nullable();
            $table->integer('year')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
