<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Storage;


Route::get('/', function () {
    return redirect('/page/movies');
});

Route::get('/test', function() {
	$url = Storage::url('images/mJ8Bj0s.jpg');
	return $url;
});

Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::get('/page/{vue_capture?}', function () {
        return response()->view('home');
    });

	Route::post('/push', 'MovieController@pushMovie');
	Route::post('/pack', 'FileController@packFile');
	Route::get('/pushdb', 'MovieController@pushDB');
	Route::post('/push/episode', 'SeriesController@pushEpisode');
	Route::post('/push/series', 'SeriesController@pushSeries');
	
	// genres
	// Route::get('/page/genres', 'GenreController@index');
	Route::get('/genres', 'GenreController@getGenres');
	Route::post('/genre', 'GenreController@addGenre');
	Route::delete('/genre/{id}', 'GenreController@deleteGenre');
	
	// upload file
	// Route::get('/page/upload', 'FileController@indexUpload');
	// Route::get('/page/files/assigned', 'FileController@indexFilesAssigned');
	// Route::get('/page/files/notassigned', 'FileController@indexFilesNotAssigned');
	Route::post('/fileupload', 'FileController@uploadFile');
	Route::get('/files/{assigned}', 'FileController@getFiles');
	Route::delete('/files/{id}', 'FileController@delete');

	// main view route
	Route::get('/home', 'HomeController@index')->name('home');
	
	/**
	 * Movie component route
	 */
	// Route::get('/page/movies', 'MovieController@index');
	Route::get('/movies', 'MovieController@getMovies');
	Route::get('/movieinfos', 'MovieController@getMovieInfos');
    Route::post('/movie', 'MovieController@addMovie');
    Route::put('/movie/{id}', 'MovieController@updateMovie');
    Route::delete('/movie/{id}', 'MovieController@deleteMovie');

    /**
	 * Series component route
	 */
	// Route::get('/page/movies', 'MovieController@index');
	Route::get('/series', 'SeriesController@getSeries');
	Route::get('/seriesinfos', 'SeriesController@getSeriesInfos');
    Route::post('/series', 'SeriesController@addSeries');
    Route::put('/series/{id}', 'SeriesController@updateSeries');
    Route::put('/series/season/{id}', 'SeriesController@updateSeason');
    Route::post('/series/{series_id}', 'SeriesController@addSeason');
    Route::delete('/series/{id}', 'SeriesController@deleteSeries');
    Route::post('/episode', 'SeriesController@addEpisode');
    Route::put('/episode/{id}', 'SeriesController@updateEpisode');
    Route::delete('/episode/{id}', 'SeriesController@deleteEpisode');
    Route::delete('/season/{id}', 'SeriesController@deleteSeason');
    Route::get('/episodesinfos', 'SeriesController@getEpisodesInfos');
});