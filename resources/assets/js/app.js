
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
import VueRouter  from 'vue-router'
import Toasted from 'vue-toasted'
import VModal from 'vue-js-modal'
import router from './router'

Vue.use(VModal)
Vue.use(Toasted)
Vue.use(VueRouter)

Vue.component('movies', require('./components/Movies.vue'));
Vue.component('uploadfilegrid', require('./components/UploadFile.vue'))
Vue.component('genres', require('./components/Genres.vue'))
Vue.component('assignedfiles', require('./components/Files.vue'))
Vue.component('unassignedfiles', require('./components/UnassignedFiles.vue'))
Vue.component('series', require('./components/Series.vue'))

const app = new Vue({
  router,
}).$mount('#app')
