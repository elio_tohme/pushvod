import VueRouter    from 'vue-router'

// lazy load components
const movies = (resolve)  => require(['./components/Movies.vue'], resolve)
const series = (resolve)  => require(['./components/Series.vue'], resolve)
const uploadfilegrid = (resolve)  => require(['./components/UploadFile.vue'], resolve)
const genres = (resolve)  => require(['./components/Genres.vue'], resolve)
const assignedfiles = (resolve)  => require(['./components/Files.vue'], resolve)
const unassignedfiles = (resolve)  => require(['./components/UnassignedFiles.vue'], resolve)

export default new VueRouter({
    mode: 'history',
    base: __dirname,
	routes: [
		{ path: '/page/movies', component: movies},
		{ path: '/page/genres', component: genres},
		{ path: '/page/upload', component: uploadfilegrid},
		{ path: '/page/files/assigned', component: assignedfiles},
		{ path: '/page/files/notassigned', component: unassignedfiles},
		{ path: '/page/series', component: series},
	]
});
